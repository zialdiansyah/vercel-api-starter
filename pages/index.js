import Head from 'next/head'
import styles from '../styles/Home.module.css'

export default function Home() {
  return (
    <div className={styles.container}>
      <Head>
        <title>Vercel API Starter</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <h1 className={styles.title}>
          Vercel API Starter
        </h1>

        <p className={styles.description}>
          Untuk API Connector bot Vutura
        </p>

        <p className={styles.description}>
          Akses <a
          href="/api"
          target="_blank"
          rel="noopener noreferrer"
        ><code className={styles.code}>/api</code></a> atau
          <a
          href="/random_quote"
          target="_blank"
          rel="noopener noreferrer"
        ><code className={styles.code}>/api/random_quote</code></a> untuk memulai
        </p>
      </main>

      <footer className={styles.footer}>
        <a
          href="https://vercel.com?utm_source=create-next-app&utm_medium=default-template&utm_campaign=create-next-app"
          target="_blank"
          rel="noopener noreferrer"
        >
          Powered by{' '}
          <img src="/vercel.svg" alt="Vercel Logo" className={styles.logo} />
        </a>
      </footer>
    </div>
  )
}
