export default (req, res) => {
  if(req.method !== 'GET') {
    return res.status(405).end(JSON.stringify({message: 'Sorry'}))
  }
  const chats = [
    {
      type: 'text',
      text: 'Hello World!'
    }
  ]

  res.status(200).end(JSON.stringify({ chats }))
}
