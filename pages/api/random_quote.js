import fetch from 'isomorphic-unfetch';

export default async (req, res) => {
  if(req.method !== 'GET') {
    return res.status(405).end(JSON.stringify({message: 'Sorry'}))
  }
  const quote = await getRandomQuote()
  const chats = [
    {
      type: 'image',
      image_url: quote.icon_url
    },
    {
      type: 'text',
      text: quote.value
    }
  ]

  res.status(200).json({ chats })
}

async function getRandomQuote() {
  const response = await fetch('https://api.chucknorris.io/jokes/random')
  const quote = await response.json()
  return quote
}
