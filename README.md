# Vercel API Starter

> Dibuat dengan Nextjs, khusus untuk di-deploy di vercel

**Case: API Connector untuk Vutura Dashboard (Element JSON API)**

### Cara running di local
1. clone project `git clone https://gitlab.com/vutura/vercel-api-starter.git [nama-project]`
2. masuk ke folder project `cd [nama-project]`
3. buat git repo di gitlab / github
4. update remote url-nya dengan url repo yang telah dibuat `git remote set-url origin https://git-repo/new-repository.git`
5. eksekusi perintah `yarn install`
6. eksekusi perintah `yarn dev` untuk running project
7. akses http://localhost:3000/api di browser, postman atau tools api lainnya
8. jangan lupa untuk commit & push ke repo git-nya agar dapat diintegrasikan dengan vercel

### Cara deploy ke Vercel
1. buat akun vercel
2. import project
3. pilih import git repository (github, gitlab atau bitbucket)
4. isi url dengan url git repo project nya
5. integrasikan vercel dengan git repo(github, gitlab atau bitbucket) kalo belum
6. klik deploy
